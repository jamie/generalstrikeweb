# General Strike Web site. 

This site is build using the [Hugo static content generator](https://gohugo.io/).

## Development

To develop, [install hugo](https://gohugo.io/getting-started/quick-start/).

Then run:

    hugo serve

And you will be able to view the site in your browser via [http://localhost:1313/](http://localhost:1313/).

## Deploy

To publish, you can run the `deploy` script (Linux or Macintosh computer).
