+++
title = "A Call to Action: Towards a General Strike to End the COVID-19 Crisis and Create a New World "
date = "2020-03-29"
description = "People over Profit: Tell the Government and Wall Street that their priority must be to Save Lives, Not Profits. Returning to Work under this Pandemic is a threat to our Collective Health and Safety"
feature = "image/page-default.webp"
+++

COVID-19 pandemic is changing the world before our very eyes. In less than 3
months, it has exposed the grotesque nature of the capitalist system to
millions, ground the world economy to a halt, and revealed how truly
interconnected our little planet really is.

As bad as this crisis is on its own terms, it is made considerably worse by the
misleadership from the White House, Congress and many state and local
governments. President Trump not only failed to heed the advice of the state's
intelligence services regarding the potential threat of the coronavirus, he
downplayed its severity for months, and has refused to mobilize the vast
resources at the disposal of the US government to address the crisis. He
continues to deny the science and proven medical advice and is now threatening
to retract social distancing orders and call for everyone to return to work by
the end of April. A bipartisan Congress just passed the largest corporate
bailout in history, that provided paltry relief to most working people in the
form of a one time payoff that won’t cover most people’s rent and utilities for
a month. The Governors of Mississippi, Florida, and Georgia have refused to
shut their states down and give clear stay at home orders to halt the spread of
the virus. And the Federal Reserve is doing everything it can to protect Wall
Street, in total disregard of the real time needs of millions of people. If
Trump and his political alliies in government, Wall Street, and the
corporations are successful in forcing a considerable number of workers to go
back to work before the pandemic has been brought under control, it will turn
into an outright calamity in the US. We cannot afford to let this happen.

This is just the tip of the iceberg. Disaster capitalism and white supremacy
are running amok. The Trump alliance of the neo-fascist right, combined with
sectors of finance capital, the fossil fuel industry and the religious right
are exploiting this crisis to accelerate climate change, reshape society and
redefine the geopolitical order. In the midst of this pandemic they have
eliminated critical environmental protection standards (reducing already poor
air quality giving new horrible life to the Black Lives Matter slogan borne of
Eric Garner’s murder, “I cant breathe” as millions struggle for air). Trump has
eliminated various health and safety standards to protect workers and
consumers, undermining unions and other working class organizations. He has
allowed genetically modified plants to be unleashed in protected lands,
expanded roundup and deportation operations, and refused to provide adequate
medical treatment of federal prisoners. Brutal bipartisan sanctions on Iran and
Venezuela prevent millions of poor and working class from accessing critical
life saving resources, and US intervention has blocked Venezuela from receiving
an IMF loan to address the COVID-19 pandemic. Right now the federal response is
being driven by finance ministers and corporations, rather than the medical
experts and front line workers directly addressing the response to the
pandemic, abandoning the potential power of a coordinated federal response. All
this is just a sample of the crimes against humanity unfolding daily at the
hands of the White House.

Despite the asymmetry of power between ourselves in the left and the organized
working class and the forces of right, we have to do everything we can to
intervene. We must stop the worst most deadly version of this pandemic from
becoming a reality, and we have to ensure that we never return to the society
that enabled this pandemic to emerge and have the impact it is having in the
first place. We must do everything that we can to create a new, just, equitable
and ecologically regenerative economy.

The question is how?

To fight back we have to use the greatest power we have at our disposal - our collective labor.

We can shut the system down to break the power of the state and capitalist
class.

We must send a clear message that things cannot and will not go back to normal.
In order to do this, we need to call for collective work and shopping
stoppages, leading to a general strike that is centered around clear,
comprehensive demands. We must make demands that will transform our broken and
inequitable society, and build a new society run by and for us - the working
class, poor, oppressed majority.

A general strike cannot be organized through online campaigns alone, or as the
result of the mere expression of a desire or even great need for a general
strike. A general strike is not organized through a list of demands, though
demands are necessary

In order for a general strike to not only take place right now, but also be
effective, we need to develop a broad united front organized around short-term
and long-term aims. We need to assess connections between unions of all sorts
and organized labor, and begin reaching out to other poor and working-class
people from within our places of work, our places of living, our places of
worship, and our places of leisure.

A general strike will also take resources to sustain. We cannot count on
capital to support this effort; they will attack and undermine us at every
turn. So, we are going to have to call for and reply upon our collective
resources. This includes our own individual purchasing power, but also the
mobilization of the collective resources at the disposal of our unions, civic
organizations, mutual aid, and spiritual institutions. We need to make sure
that we can provide aid to workers on the frontlines of the health struggle and
the frontlines of the supply chain struggles. This means providing mutual aid
where warranted, as well as strike funds to support workers from losing their
homes, cars, medical care, and other essential expenses.

Those with the most experience in organizing strikes of all sorts – both young
and old – must step up in this moment and provide general insights and
strategies that can be utilized by the united front in tandem with organized
labor groups that are on the same page, and these insights in addition to
strategy must inform an open information campaign that not only brings
attention to strike efforts, but brings in supporters from outside of our
organized formations who can then employ a wide range of strategies to begin
initiating mass actions without feeling isolated.

The capitalists and landlords win when we are divided, fearful, and/or fighting
our own battles in isolation. All it takes is enough of us breaking for them to
have their way. We stand a much better chance coordinating nationally and
internationally, and with organizing networks and infrastructure that are
fortified with centuries’ worth of cumulative experience between the organizers
who comprise them. We also stand a better chance with global attention.

Those who control the land, the property, and the businesses want you to
believe that this COVID-19 crisis is going to blow over soon, and that everyone
will simply go back to work. They want you to believe that things will return
to “normal” within a matter of months, and even weeks. Right now, poor and
working-class people have an opportunity to make it clear to the ruling classes
that not only was “normal” abnormal to begin with, but that we are not going to
settle for a return to the social and economic conditions that created this
pandemic to begin with.

We should take inspiration in that we are not alone in calling for and acting
upon a call for a general strike. Workers throughout the country and the world
are spontaneously taking matters into their own hands. Auto workers, chicken
factory workers, nurses, drivers, grocery store workers, and more are all
taking independent action. Calls for a rent strike are going viral, as working
poor and homeless workers are starting to occupy hundreds of vacant homes to
meet their needs and practice the necessary social distancing to ensure their
survival. Things are in motion and we need to build upon this momentum,
quickly.

This crisis changes everything.

We have an opportunity to take control now, and we are ready to fight for a
society in which all people can live with full autonomy without having to worry
about survival.

[See the basic framing and list of preliminary demands](/framing) that we think are
essential to call for and act upon at this time.
