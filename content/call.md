+++
title = "General Strike! No Work, No Shopping Friday, May 1st"
description = "People over Profit: Tell the Government and Wall Street that their priority must be to Save Lives, Not Profits. Returning to Work under this Pandemic is a threat to our Collective Health and Safety"
feature = "image/fist.jpg"
+++

People over Profit: Tell the Government and Wall Street that their priority
must be to Save Lives, Not Profits. Returning to Work under this Pandemic is a
threat to our Collective Health and Safety

We Need Systems Change, Not Just Relief and Reform. The Capitalist System Can’t
Resolve this Crisis.

Our Demands:

* Protect All Frontline Workers in the Hospitals, the Supply Chains, and the Farms and Fields to ensure that they have all of the equipment and disinfectant materials that they need to keep themselves and the general public healthy
* Protect Asians and other vulnerable communities, including the homeless, migrants, and refugees from discrimination and attack in this time of crisis
* Democratize the Means of Production, Convert the Corporations and Workplaces into Cooperatives to produce what we need and distribute equitably according to need
* Institute Universal Health Care Now
* Institute Universal Basic Services Now (Education, Childcare, Elderly Care, Water, Electricity, Internet, etc.) based on Economic, Social and Cultural rights guidelines
* Institute Universal Basic Income Now
* Democratize the Finance, Credit and Insurance Industries - Bailout the People, Not the Corporations and Wall Street
* Decarbonize the Economy, Institute a Green New Deal based on a Just Transition, End the Fossil Fuel and Extractive Industries Now
* Housing is a Human Right, Decommodify Housing Now, Open all available housing stock to those who need it now
* Ensure there is clean drinking water for all communities, decommodify water now
* Cancel Our Debts, Institute a Debt Jubilee Now
* Close the Jails, Close the Prisons, Release the Prisoners
* Close the Detention Centers, Reunite the Families, Stop the Raids and Deportations
* Close all of the Overseas Military Bases, Cut the Military (Defense) and Spy (Surveillance) Budgets and Redirect these funds to Health Care, Social Services, Universal Basic Income and Greening Public Infrastructure and the Economy

We are calling upon all who agree with this call to join us in calling for militant action to shut the system down. **This is what we are asking you to do immediately:**

1. Let us know if you agree with this call and this list of demands, or how you would add upon or strengthen them.
1. Let us know if you would be willing to participate in a coordinating body to help organize and advance this call. This coordinating body would take on the task of building out the base of the united front, help facilitate community between its constituent parts, and facilitate the calls to action.
1. Join us for our first zoom call on Monday, April 6th at 12 pm est/11 am cst/10 am mst/9 am pst to start building this front and advancing this call to action. To participate in the call and communicate your alignment and willingness help coordinate a broad, united front initiative email us at **GenStrike2020@protonmail.com**.

Finally, this initiative is not intended to negate any of the calls already
issued for a rent strike, a people’s bailout, etc. We hope to unite all who can
be united, while respecting the independence of initiative of the various
forces that would comprise the front. We have to apply ceaseless, unyielding
pressure on the system and the forces that enable it. Let’s do so with any eye
towards employing maximum unity to end this crisis and create a new world in
its aftermath. 
